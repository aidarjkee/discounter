//
//  RequestBuilder.swift
//  Discounter
//
//  Created by Aidar Fatkhutdinov on 27.02.2021.
//

import Foundation

protocol RequestBuilder {
    var urlRequest: URLRequest {get}
}
