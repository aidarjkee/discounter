//
//  APIError.swift
//  Discounter
//
//  Created by Aidar Fatkhutdinov on 27.02.2021.
//

import Foundation

enum APIError: Error {
    case decodingError
    case httpError(Int)
    case unknown
}
