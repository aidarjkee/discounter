//
//  APIService.swift
//  Discounter
//
//  Created by Aidar Fatkhutdinov on 27.02.2021.
//

import Combine
import UIKit

protocol APIService {
    func request<T: Decodable>(with builder: RequestBuilder) -> AnyPublisher<T, APIError>
}
