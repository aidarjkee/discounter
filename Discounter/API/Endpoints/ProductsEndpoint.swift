//
//  ProductsEndpoint.swift
//  Discounter
//
//  Created by Aidar Fatkhutdinov on 27.02.2021.
//

import Foundation

enum ProductsEndpoint {
    case getProducts(catalog: String, page: Int, lastProductDate: String?)
}

extension ProductsEndpoint: RequestBuilder {
    var urlRequest: URLRequest {
        switch self {
        case .getProducts(let catalog, let page, let lastProductDate):
            var urlComponents = URLComponents(string: "\(Constants.server)/api/v1/products")
            
            var queryItems: [URLQueryItem] = [
                URLQueryItem(name: "page", value: "\(page)"),
                URLQueryItem(name: "catalog", value: "\(catalog)"),
                URLQueryItem(name: "status", value: "active")
            ]
            
            if let lastProductDate = lastProductDate {
                print(lastProductDate)
                queryItems.append(URLQueryItem(name: "lastProductDate", value: lastProductDate))
            }
            
            urlComponents?.queryItems = queryItems
            
            guard let url = urlComponents?.url else { preconditionFailure("Invalid URL format") }
            
            var request = URLRequest(url: url)
            request.httpMethod = "GET"
            
            return request
        }
    }
}
