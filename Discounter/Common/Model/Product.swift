//
//  Product.swift
//  Discounter
//
//  Created by Aidar Fatkhutdinov on 13.02.2021.
//

import SwiftUI

struct Product: Identifiable, Hashable, Codable {
    let id: String
    let name: String
    let imageLink: String
    let price: Int
    let oldPrice: Int
    let discount: Int
    let size: Size
    let productLink: String
    let catalog: String
    let createdAt: String
    
    let formatedPrice: String
    let formatedOldPrice: String
    let formatedSizes: String
    
    enum CodingKeys: String, CodingKey {
        case id = "_id"
        case name
        case imageLink
        case price
        case oldPrice
        case discount
        case size
        case productLink
        case createdAt
        case catalog
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        
        id = try values.decode(String.self, forKey: .id)
        name = try values.decode(String.self, forKey: .name)
        imageLink = try values.decode(String.self, forKey: .imageLink)
        price = try values.decode(Int.self, forKey: .price)
        oldPrice = try values.decode(Int.self, forKey: .oldPrice)
        discount = try values.decode(Int.self, forKey: .discount)
        size = try values.decode(Size.self, forKey: .size)
        productLink = try values.decode(String.self, forKey: .productLink)
        createdAt = try values.decode(String.self, forKey: .createdAt)
        catalog = try values.decode(String.self, forKey: .catalog)
        
        formatedPrice = "\(price.formattedWithSeparator) ₽"
        formatedOldPrice = "\(oldPrice.formattedWithSeparator) ₽"
        formatedSizes = size.country + " " + size.sizes.reduce("", { $0 + $1 + " " })
    }
    
    struct Size: Hashable, Codable {
        let country: String
        let sizes: [String]
    }
}
