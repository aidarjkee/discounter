//
//  Catalog.swift
//  Discounter
//
//  Created by Aidar Fatkhutdinov on 27.02.2021.
//

import SwiftUI

struct Catalog {
    let name: String
    let id: String
}
