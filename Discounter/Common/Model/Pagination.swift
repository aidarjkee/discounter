//
//  Pagination.swift
//  Discounter
//
//  Created by Aidar Fatkhutdinov on 27.02.2021.
//

import SwiftUI

struct Pagination: Decodable {
    let next: Page?
    let prev: Page?
    
    struct Page: Decodable {
        let page: Int
        let limit: Int
    }
}
