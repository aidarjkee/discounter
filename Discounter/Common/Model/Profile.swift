//
//  Profile.swift
//  Discounter
//
//  Created by Aidar Fatkhutdinov on 27.02.2021.
//

import SwiftUI

struct Profile: Codable {
    var lastProducts: [LastProduct]
    
    struct LastProduct: Codable {
        let catalogId: String
        var lastProductsDate: String
    }
}
