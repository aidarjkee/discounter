//
//  ProductCardView.swift
//  Discounter
//
//  Created by Aidar Fatkhutdinov on 13.02.2021.
//

import SwiftUI
import Kingfisher

struct ProductCardView: View {
    let product: Product
    let width: CGFloat
    let height: CGFloat
    let isFavorite: Bool
    var columnsType: ColumnsType = .x2
    
    var favoriteButtonTapped: ((Product) -> Void)
    
    var body: some View {
        VStack(alignment: .leading, spacing: 0) {
            ZStack(alignment: .topTrailing) {
                KFImage(URL(string: product.imageLink))
                    .resizable()
                    .scaledToFill()
                    .frame(width: width, height: height)
                    .clipped()
                    .contentShape(Rectangle())
                
                VStack {
                    Spacer(minLength: 0)
                    
                    Text("-\(product.discount)%")
                        .font(.caption2)
                        .foregroundColor(.white)
                        .padding(4)
                        .background(Color.red)
                }
                
                Button(action: {
                    let feedbackGenerator = UISelectionFeedbackGenerator()
                    feedbackGenerator.selectionChanged()
                    
                    favoriteButtonTapped(product)
                }) {
                    Image(systemName: isFavorite ? "heart.fill" : "heart")
                        .foregroundColor(.black)
                        .padding(12)
                    
                }
            }
            .frame(height: height)
            
            VStack(alignment: .leading, spacing: 4) {
                Text(product.name)
                    .font(.headline)
                    .lineLimit(2)
                
                Group {
                    if columnsType == ColumnsType.x1 || columnsType == ColumnsType.x2 {
                        HStack(spacing: 6) {
                            Text(product.formatedPrice)
                                .bold()
                            
                            Text(product.formatedOldPrice)
                                .foregroundColor(.secondary)
                                .strikethrough()
                        }
                    } else {
                        Text(product.formatedPrice)
                            .bold()
                        
                        Text(product.formatedOldPrice)
                            .foregroundColor(.secondary)
                            .strikethrough()
                    }
                }
                .font(.footnote)
                
                Text(product.formatedSizes)
                    .font(.footnote)
                    .foregroundColor(.secondary)
                    .lineLimit(2)
                
                Spacer(minLength: 0)
            }
            .padding(.vertical)
            .padding(.horizontal, 12)
            
        }
    }
}
