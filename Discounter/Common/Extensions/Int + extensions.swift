//
//  Int + extensions.swift
//  Discounter
//
//  Created by Aidar Fatkhutdinov on 14.02.2021.
//

import SwiftUI

extension Int {
    func getNumEnding(words: [String]) -> String { // words - массив слов или окончаний для чисел (1, 4, 5)
        var finalWord: String
        var i: Int
        
        let count = self % 100
        
        if (count >= 11 && count <= 19) {
            finalWord = words[2]
        } else {
            i = count % 10
            
            switch (i) {
            case (1): finalWord = words[0]
            case (2...4): finalWord = words[1]
            default: finalWord = words[2]
            }
        }
        return finalWord
    }
}
