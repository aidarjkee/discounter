//
//  Numeric + extensions.swift
//  Discounter
//
//  Created by Aidar Fatkhutdinov on 13.02.2021.
//

import SwiftUI

extension Numeric {
    var formattedWithSeparator: String { Formatter.withSeparator.string(for: self) ?? "" }
}
