//
//  FavoritesManager.swift
//  Discounter
//
//  Created by Aidar Fatkhutdinov on 13.02.2021.
//

import SwiftUI

final class FavoritesManager: ObservableObject {
    @Published var products: [Product]
    
    private let key = "FavoritesManager"
    
    init() {
        if let data = UserDefaults.standard.data(forKey: key), let products = try? JSONDecoder().decode([Product].self, from: data) {
            self.products = products
        } else {
            self.products = []
        }
    }
    
    func isFavorite(product: Product) -> Bool {
        products.contains(where: { $0.id == product.id })
    }
    
    func add(product: Product) {
        products.append(product)
        save()
    }
    
    func remove(product: Product) {
        if let index = products.firstIndex(where: { $0.id == product.id }) {
            products.remove(at: index)
            save()
        }
    }
    
    func removeAll() {
        products.removeAll()
        save()
    }
    
    func save() {
        if let products = try? JSONEncoder().encode(products) {
            UserDefaults.standard.set(products, forKey: key)
            UserDefaults.standard.synchronize()
        }
    }
}
