//
//  ProfileManager.swift
//  Discounter
//
//  Created by Aidar Fatkhutdinov on 27.02.2021.
//

import SwiftUI

final class ProfileManager: ObservableObject {
    @Published var profile: Profile
    
    private let key = "ProfileManager"
    
    init() {
        if let data = UserDefaults.standard.data(forKey: key), let profile = try? JSONDecoder().decode(Profile.self, from: data) {
            self.profile = profile
        } else {
            self.profile = Profile(lastProducts: [])
        }
    }
    
    func updateLastProduct(lastProduct: Profile.LastProduct) {
        updateLastProducts(lastProduct: lastProduct)
        save()
    }
    
    func getLastProduct(catalogId: String) -> Profile.LastProduct? {
        return profile.lastProducts.first(where: { $0.catalogId == catalogId })
    }
}

extension ProfileManager {
    private func updateLastProducts(lastProduct: Profile.LastProduct) {
        if let index = profile.lastProducts.firstIndex(where: { $0.catalogId == lastProduct.catalogId }) {
            profile.lastProducts[index].lastProductsDate = lastProduct.lastProductsDate
        } else {
            profile.lastProducts.append(lastProduct)
        }
    }
    
    private func save() {
        if let profile = try? JSONEncoder().encode(profile) {
            UserDefaults.standard.set(profile, forKey: key)
            UserDefaults.standard.synchronize()
        }
    }
}
