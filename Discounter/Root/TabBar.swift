//
//  TabBar.swift
//  Discounter
//
//  Created by Aidar Fatkhutdinov on 01.03.2021.
//

import SwiftUI

struct TabBar: View {
    @State var selectedView = 1
    
    var body: some View {
        TabView(selection: $selectedView) {
            NavigationView {
                CatalogView()
                    .navigationBarTitleDisplayMode(.inline)
            }
            .tabItem {
                Label(title: { Text("Каталог") },
                      icon: { Image(systemName: selectedView == 1 ? "square.grid.2x2.fill" : "square.grid.2x2") })
                    .foregroundColor(.primary)
            }
            .tag(1)
            
            NavigationView {
                FavoritesView()
                    .navigationBarTitleDisplayMode(.inline)
            }
            .tabItem {
                Label(title: { Text("Избранное") },
                      icon: { Image(systemName: selectedView == 2 ? "heart.fill" : "heart") })
                    .foregroundColor(.primary)
            }
            .tag(2)
        }
    }
}
