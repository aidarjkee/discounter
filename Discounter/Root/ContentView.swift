//
//  ContentView.swift
//  Discounter
//
//  Created by Aidar Fatkhutdinov on 13.02.2021.
//

import SwiftUI

struct ContentView: View {
    @ObservedObject var favorites = FavoritesManager()
    
    @State var isOnboardingViewDisplayed = false
    
    init() {
        UINavigationBar.appearance().tintColor = UIColor(Color.primary)
    }
    
    var body: some View {
        TabBar()
            .onAppear {
                willDisplayOnboardingView()
            }
            .sheet(isPresented: $isOnboardingViewDisplayed, content: {
                OnboardingView()
            })
            .accentColor(.primary)
            .environmentObject(favorites)
    }
    
    private func willDisplayOnboardingView() {
        let launchedBefore = UserDefaults.standard.bool(forKey: "launchedBefore")
        if !launchedBefore  {
            UserDefaults.standard.set(true, forKey: "launchedBefore")
            UserDefaults.standard.synchronize()
            
            isOnboardingViewDisplayed = true
        }
    }
}
