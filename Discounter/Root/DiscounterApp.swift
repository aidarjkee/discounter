//
//  DiscounterApp.swift
//  Discounter
//
//  Created by Aidar Fatkhutdinov on 13.02.2021.
//

import SwiftUI

@main
struct DiscounterApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
