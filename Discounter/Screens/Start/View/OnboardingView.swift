//
//  OnboardingView.swift
//  Discounter
//
//  Created by Aidar Fatkhutdinov on 14.02.2021.
//

import SwiftUI

struct OnboardingView: View {
    @Environment(\.colorScheme) var colorScheme: ColorScheme
    @Environment(\.presentationMode) var presentationMode
    
    var body: some View {
        ZStack {
            VStack(spacing: 0) {
                Text("Как работает Discounter?")
                    .font(.largeTitle)
                    .bold()
                    .multilineTextAlignment(.center)
                    .padding(.bottom, 20)
                
                Spacer(minLength: 0)
                
                Button(action: {
                    presentationMode.wrappedValue.dismiss()
                }) {
                    Text("Продолжить")
                        .font(.headline)
                        .frame(height: 50)
                        .frame(maxWidth: .infinity)
                        .foregroundColor(colorScheme == .light ? .white : .black)
                        .background(Color(colorScheme == .light ? .black : .white))
                        .cornerRadius(14)
                }
            }
            
            VStack(spacing: 30) {
                OnboardingRow(icon: "1",
                              title: "Title 1",
                              subTitle: "subTitle 1",
                              color: Color.pink)
                
                OnboardingRow(icon: "2",
                              title: "Title 2",
                              subTitle: "subTitle 2",
                              color: Color(#colorLiteral(red: 0.3529411765, green: 0.7843137255, blue: 0.9803921569, alpha: 1)))
                
                OnboardingRow(icon: "3",
                              title: "Title 3",
                              subTitle: "subTitle 3",
                              color: Color.green)
            }
            .padding(.bottom, UIScreen.main.bounds.height * 0.175)
        }
        .padding(.bottom, 50)
        .padding(.top, 80)
        .padding(.horizontal, 30)
    }
}
