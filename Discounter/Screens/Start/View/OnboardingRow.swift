//
//  OnboardingRow.swift
//  Discounter
//
//  Created by Aidar Fatkhutdinov on 14.02.2021.
//

import SwiftUI

struct OnboardingRow: View {
    let icon: String
    let title: String
    let subTitle: String
    let color: Color
    
    var body: some View {
        HStack(spacing: 20) {
            Image(systemName: icon)
                .foregroundColor(.white)
                .font(.title3)
                .frame(width: 40, height: 40)
                .background(color)
                .clipShape(Circle())
            
            VStack(alignment: .leading, spacing: 2) {
                Text(title)
                    .font(.headline)
                    .bold()
                
                Text(subTitle)
                    .font(.callout)
                    .foregroundColor(.secondary)
            }
        }
    }
}
