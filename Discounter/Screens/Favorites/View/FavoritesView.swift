//
//  FavoritesView.swift
//  Discounter
//
//  Created by Aidar Fatkhutdinov on 13.02.2021.
//

import SwiftUI

struct FavoritesView: View {
    @ObservedObject var viewModel = FavoritesViewModel.shared
    
    @EnvironmentObject var favorites: FavoritesManager
    
    @Environment(\.openURL) var openURL
    
    var body: some View {
        Group {
            if favorites.products.isEmpty {
                VStack(alignment: .center, spacing: 8) {
                    Text("Пока пусто!")
                        .font(.title2)
                        .bold()
                        .foregroundColor(.primary)
                    
                    Text("Сохраняйте понравившиеся вещи в «Избранное», чтобы больше не тратить время на их поиск.")
                        .multilineTextAlignment(.center)
                        .foregroundColor(.secondary)
                }
                .padding(.horizontal)
            } else {
                GeometryReader { reader in
                    ScrollView(showsIndicators: false) {
                        LazyVGrid(columns: viewModel.columns, spacing: 0) {
                            ForEach(favorites.products.reversed()) { product in
                                ProductCardView(product: product,
                                                width: reader.size.width / CGFloat(viewModel.columns.count),
                                                height: reader.size.width / 1.5,
                                                isFavorite: favorites.isFavorite(product: product),
                                                favoriteButtonTapped: { product in
                                                    if favorites.isFavorite(product: product) { favorites.remove(product: product) }
                                                    else { favorites.add(product: product) }
                                                })
                                    .onTapGesture {
                                        if let link = product.productLink.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) {
                                            if let url = URL(string: link) {
                                                openURL(url)
                                            }
                                        }
                                    }
                            }
                        }
                    }
                }
            }
        }
        .toolbar(content: {
            ToolbarItem(placement: .principal, content: {
                VStack {
                    Text("Избранное")
                        .font(.headline)
                    
                    if !favorites.products.isEmpty {
                        Text("\(favorites.products.count) \(favorites.products.count.getNumEnding(words: ["товар", "товара", "товаров"]))")
                            .font(.subheadline)
                            .foregroundColor(.secondary)
                    }
                }
            })
            
            ToolbarItemGroup(placement: .navigationBarTrailing) {
                if !favorites.products.isEmpty {
                    Button(action: {
                        favorites.removeAll()
                    }) {
                        Text("Очистить")
                    }
                }
            }
        })
    }
}
