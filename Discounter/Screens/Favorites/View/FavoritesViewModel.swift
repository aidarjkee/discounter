//
//  FavoritesViewModel.swift
//  Discounter
//
//  Created by Aidar Fatkhutdinov on 13.02.2021.
//

import SwiftUI

final class FavoritesViewModel: ObservableObject {
    let columns = [GridItem(.flexible()), GridItem(.flexible())]
    
    static let shared = FavoritesViewModel()
}
