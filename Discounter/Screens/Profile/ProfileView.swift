//
//  ProfileView.swift
//  Discounter
//
//  Created by Aidar Fatkhutdinov on 15.02.2021.
//

import SwiftUI

struct ProfileView: View {
    @State private var showShareSheet = false
    
    var body: some View {
        Form {
            Section {
                NavigationLink(destination: FeedbackView()) {
                    Text("Обратная связь")
                }
                
                Button("Поделиться приложением") {
                    showShareSheet = true
                }
                .foregroundColor(.primary)
            }
            
            Section {
                HStack {
                    Text("Версия")
                    
                    Spacer()
                    
                    Text(Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String ?? "")
                        .foregroundColor(.secondary)
                }
            }
        }
        .sheet(isPresented: $showShareSheet) { ShareSheet(activityItems: ["Hello World"]) }
        .toolbar(content: {
            ToolbarItem(placement: .principal) {
                Text("Профиль").font(.headline)
            }
        })
    }
}
