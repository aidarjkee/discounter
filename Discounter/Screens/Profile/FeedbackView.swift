//
//  FeedbackView.swift
//  Discounter
//
//  Created by Aidar Fatkhutdinov on 16.02.2021.
//

import SwiftUI

struct FeedbackView: View {
    @Environment(\.presentationMode) var presentationMode
    
    @State var text = ""
    
    var body: some View {
        Form {
            TextField("Ваши вопросы и предложения", text: $text).accentColor(.primary)
        }
        .toolbar(content: {
            ToolbarItem(placement: .principal, content: {
                Text("Обратная связь")
                    .font(.headline)
            })
            
            ToolbarItem(placement: .navigationBarTrailing) {
                if !text.isEmpty {
                    Button(action: {
                        presentationMode.wrappedValue.dismiss()
                    }) {
                        Text("Отправить")
                    }
                }
            }
        })
    }
}
