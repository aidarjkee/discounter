//
//  CatalogViewModel.swift
//  Discounter
//
//  Created by Aidar Fatkhutdinov on 13.02.2021.
//

import SwiftUI
import Combine

final class CatalogViewModel: ObservableObject, ProductsService {
    @ObservedObject var profileManager = ProfileManager()
    
    @Published var products: [Product] = []
    @Published var columns = [GridItem(.flexible()), GridItem(.flexible())]
    @Published var pickerMode = 0 {
        didSet {
            products.removeAll()
            isFirstLoadingDone = false
            isCountAlertDisplayed = false
            getProducts()
        }
    }
    @Published var columnsType: ColumnsType = .x2 {
        didSet {
            switch columnsType {
            case .x1: columns = [GridItem(.flexible())]
            case .x2: columns = [GridItem(.flexible()), GridItem(.flexible())]
            case .x3: columns = [GridItem(.flexible()), GridItem(.flexible()), GridItem(.flexible())]
            }
        }
    }
    
    @Published var isFirstLoadingDone = false
    @Published var isProfileViewDisplayed = false
    @Published var isCountAlertDisplayed = false
    
    var isReadyToLoad = true
    var pagination: Pagination?
    var newProductCount = 0
    
    let catalog = [Catalog(name: "Одежда", id: "5f197b7462cfab0c05b9c5bf"),
                   Catalog(name: "Обувь", id: "5f1819b1c5e53509094c44d3"),
                   Catalog(name: "Аксессуары", id: "5f1819b1c5e53509094c44d4")]
    
    var apiSession: APIService = APISession()
    
    var cancellables = Set<AnyCancellable>()
    
    static let shared = CatalogViewModel()
}

extension CatalogViewModel {
    func start() {
        if products.isEmpty { // products.isEmpty = isFirstLoadingDone
            getProducts()
        }
    }
    
    func getProducts(page: Int = 1) {
        guard isReadyToLoad else { return }
        
        isReadyToLoad = false
        
        let cancellable = self.getProducts(catalog: catalog[pickerMode].id,
                                           page: page,
                                           lastProductDate: profileManager.getLastProduct(catalogId: catalog[pickerMode].id)?.lastProductsDate)
            .sink(receiveCompletion: { result in
                switch result {
                case .failure(let error): print("Handle getProducts error: \(error)")
                case .finished: break
                }
                self.isReadyToLoad = true
            }) { (result) in
                self.pagination = result.pagination
                self.newProductCount = result.newProductCount
                self.products += result.data
                
                if let lastProduct = self.products.first {
                    self.profileManager.updateLastProduct(lastProduct: Profile.LastProduct(catalogId: lastProduct.catalog,
                                                                                lastProductsDate: lastProduct.createdAt))
                }
                
                if !self.isFirstLoadingDone {
                    if result.newProductCount > 0 {
                        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) { withAnimation { self.isCountAlertDisplayed = true } }
                        
                        DispatchQueue.main.asyncAfter(deadline: .now() + 3.5) { withAnimation { self.isCountAlertDisplayed = false } }
                    }
                }
                
                self.isFirstLoadingDone = true
            }
        cancellables.insert(cancellable)
    }
    
    func getNewPage() {
        if let page = pagination?.next?.page {
            getProducts(page: page)
        }
    }
    
    func changeColumnsType() {
        switch columnsType {
        case .x1: columnsType = .x2
        case .x2: columnsType = .x3
        case .x3: columnsType = .x1
        }
    }
}
