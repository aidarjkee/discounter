//
//  CatalogView.swift
//  Discounter
//
//  Created by Aidar Fatkhutdinov on 13.02.2021.
//

import SwiftUI

struct CatalogView: View {
    @ObservedObject var viewModel = CatalogViewModel.shared
    
    @EnvironmentObject var favorites: FavoritesManager
    
    @Environment(\.openURL) var openURL
    
    @Environment(\.colorScheme) var colorScheme: ColorScheme
    
    var body: some View {
        GeometryReader { reader in
            ZStack {
                if !viewModel.isFirstLoadingDone { ProgressView() }
                
                ScrollView(showsIndicators: false) {
                    LazyVGrid(columns: viewModel.columns, spacing: 0) {
                        ForEach(Array(viewModel.products.enumerated()), id: \.offset) { index, product in
                            ProductCardView(product: product,
                                            width: reader.size.width / CGFloat(viewModel.columns.count),
                                            height: reader.size.width / (viewModel.columnsType == ColumnsType.x2 ? 1.5 : CGFloat(viewModel.columns.count)),
                                            isFavorite: favorites.isFavorite(product: product),
                                            columnsType: viewModel.columnsType,
                                            favoriteButtonTapped: { product in
                                                if favorites.isFavorite(product: product) { favorites.remove(product: product) }
                                                else { favorites.add(product: product) }
                                            })
                                .onAppear {
                                    if index == viewModel.products.count - 4 {
                                        viewModel.getNewPage()
                                    }
                                }
                                .onTapGesture {
                                    if let link = product.productLink.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) {
                                        if let url = URL(string: link) {
                                            openURL(url)
                                        }
                                    }
                                }
                        }
                    }
                }
                
                VStack(spacing: 0) {
                    Spacer(minLength: 0)
                    
                    HStack(spacing: 0) {
                        Spacer(minLength: 0)
                        
                        Button(action: {
                            let feedbackGenerator = UISelectionFeedbackGenerator()
                            feedbackGenerator.selectionChanged()
                            
                            viewModel.changeColumnsType()
                        }) {
                            Group {
                                switch viewModel.columnsType {
                                case .x1: Image(systemName: "square.grid.2x2")
                                case .x2: Image(systemName: "square.grid.3x2")
                                case .x3: Image(systemName: "square")
                                }
                            }
                            .foregroundColor(.white)
                            .frame(width: 40, height: 40)
                            .background(Color.black)
                        }
                        .opacity(0.7)
                        .clipShape(Circle())
                        .frame(width: 40, height: 40)
                        .padding()
                    }
                }
                
                VStack(spacing: 0) {
                    Text("В каталоге \(viewModel.catalog[viewModel.pickerMode].name) \(viewModel.newProductCount) \(viewModel.newProductCount.getNumEnding(words: ["новый товар", "новых товара", "новых товаров"]))")
                        .padding(.horizontal)
                        .frame(height: 40)
                        .background(colorScheme == .dark ? Color.black : Color.white)
                        .foregroundColor(.primary)
                        .font(.footnote)
                        .clipShape(Capsule())
                        .shadow(color: Color(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.1)), radius: 20, x: 0, y: 0)
                        .opacity(viewModel.isCountAlertDisplayed ? 1 : 0)
                        .padding(.top, viewModel.isCountAlertDisplayed ? 16 : -(16 + 40)) // 16 padding 40 height of alert
                    
                    Spacer(minLength: 0)
                }
            }
        }
        .onAppear {
            viewModel.start()
        }
        .sheet(isPresented: $viewModel.isProfileViewDisplayed) { NavigationView { ProfileView().navigationBarTitleDisplayMode(.inline) } }
        .toolbar(content: {
            ToolbarItem(placement: .principal) {
                Picker("CatalogPicker", selection: $viewModel.pickerMode) {
                    Text(viewModel.catalog[0].name).tag(0)
                    Text(viewModel.catalog[1].name).tag(1)
                    Text(viewModel.catalog[2].name).tag(2)
                }
                .pickerStyle(SegmentedPickerStyle())
            }
            
            ToolbarItem(placement: .navigationBarTrailing) {
                Button(action: {
                    viewModel.isProfileViewDisplayed = true
                }) {
                    Image(systemName: "person.crop.circle")
                }
            }
        })
    }
}
