//
//  GetProductsResponse.swift
//  Discounter
//
//  Created by Aidar Fatkhutdinov on 27.02.2021.
//

import Foundation

struct GetProductsResponse: Decodable {
    let success: Bool
    let count: Int
    let newProductCount: Int
    let pagination: Pagination
    let data: [Product]
}
