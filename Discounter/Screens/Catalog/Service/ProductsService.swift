//
//  ProductsService.swift
//  Discounter
//
//  Created by Aidar Fatkhutdinov on 27.02.2021.
//

import Combine
import UIKit

protocol ProductsService {
    var apiSession: APIService {get}
    
    func getProducts(catalog: String, page: Int, lastProductDate: String?) -> AnyPublisher<GetProductsResponse, APIError>
}

extension ProductsService {
    
    func getProducts(catalog: String, page: Int, lastProductDate: String?) -> AnyPublisher<GetProductsResponse, APIError> {
        return apiSession.request(with: ProductsEndpoint.getProducts(catalog: catalog, page: page, lastProductDate: lastProductDate)).eraseToAnyPublisher()
    }
}
